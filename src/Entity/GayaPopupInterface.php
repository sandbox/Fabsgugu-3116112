<?php

namespace Drupal\gaya_popup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Popup Messages Entity entities.
 *
 * @ingroup gaya_popup
 */
interface GayaPopupInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Popup Messages Entity name.
   *
   * @return string
   *   Name of the Popup Messages Entity.
   */
  public function getName();

  /**
   * Sets the Popup Messages Entity name.
   *
   * @param string $name
   *   The Popup Messages Entity name.
   *
   * @return \Drupal\gaya_popup\Entity\GayaPopupInterface
   *   The called Popup Messages Entity entity.
   */
  public function setName($name);

  /**
   * Gets the Popup Messages Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Popup Messages Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Popup Messages Entity creation timestamp.
   *
   * @param int $timestamp
   *   The Popup Messages Entity creation timestamp.
   *
   * @return \Drupal\gaya_popup\Entity\GayaPopupInterface
   *   The called Popup Messages Entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Popup Messages Entity published status indicator.
   *
   * Unpublished Popup Messages Entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Popup Messages Entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Popup Messages Entity.
   *
   * @param bool $published
   *   TRUE to set this Popup Messages Entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\gaya_popup\Entity\GayaPopupInterface
   *   The called Popup Messages Entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Popup Messages Entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Popup Messages Entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\gaya_popup\Entity\GayaPopupInterface
   *   The called Popup Messages Entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Popup Messages Entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Popup Messages Entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\gaya_popup\Entity\GayaPopupInterface
   *   The called Popup Messages Entity entity.
   */
  public function setRevisionUserId($uid);

}
