<?php

namespace Drupal\gaya_popup\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for gaya_popup_entity entity.
 *
 * @ingroup gaya_popup_entity
 */
class MessagesListBuilder extends EntityListBuilder {
    /**
     * The url generator.
     *
     * @var \Drupal\Core\Routing\UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * {@inheritdoc}
     */
    public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
        return new static(
            $entity_type,
            $container->get('entity.manager')->getStorage($entity_type->id()),
            $container->get('url_generator')
        );
    }
    /**
     * Constructs a new ContactListBuilder object.
     *
     * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
     *   The entity type definition.
     * @param \Drupal\Core\Entity\EntityStorageInterface $storage
     *   The entity storage class.
     * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
     *   The url generator.
     */
    public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
        parent::__construct($entity_type, $storage);
        $this->urlGenerator = $url_generator;
    }

    /**
     * {@inheritdoc}
     *
     * We override ::render() so that we can add our own content above the table.
     * parent::render() is where EntityListBuilder creates the table using our
     * buildHeader() and buildRow() implementations.
     */
    public function render() {
        $build['description'] = [
            '#markup' => $this->t('<a href="@adminlink">Draggable view for alerts entities</a>', array(
                '@adminlink' => \Drupal::urlGenerator()
                    ->generateFromRoute('gaya_popup.popup_entity_settings'),
            )),
        ];

        $build['table'] = parent::render();
        return $build;
    }

    /**
     * {@inheritdoc}
     *
     * Building the header and content lines for the Popup Entities list.
     *
     * Calling the parent::buildHeader() adds a column for the possible actions
     * and inserts the 'edit' and 'delete' links as defined for the entity type.
     */
    public function buildHeader() {
        $header['id'] = $this->t('ID');
        $header['title'] = $this->t('Title');
        $header['body'] = $this->t('Message');
        $header['enabled'] = $this->t('Enabled');
        $header['field_frequency'] = $this->t('Frequency');
        $header['field_area'] = $this->t('Area');
        $header['field_closed'] = $this->t('Do not show if closed');
        $header['field_date_on'] = $this->t('Date begin');
        $header['field_date_off'] = $this->t('Date end');
        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity) {
        /* @var $entity \Drupal\gaya_popup\Entity\PopupMessageEntity */
        $datetime_on = "";
        if(!empty($entity->field_date_on->value)){
            $datetime_on = strtotime($entity->field_date_on->value);
            $datetime_on = \Drupal::service('date.formatter')->format($datetime_on, 'custom', 'Y-m-d');
        }
        $datetime_off = "";
        if(!empty($entity->field_date_off->value)){
            $datetime_off = strtotime($entity->field_date_off->value);
            $datetime_off = \Drupal::service('date.formatter')->format($datetime_off, 'custom', 'Y-m-d');
        }

        $row['id'] = $entity->id();
        $row['title'] = $entity->link();
        $row['body'] = $entity->body->value;
        $row['enabled'] = ($entity->enabled->value) ? $this->t("Yes") : $this->t("No");
        $row['field_frequency'] = ($entity->field_frequency->value == 'repeat') ? $this->t("Everytime") : $this->t("Once in session");
        $row['field_area'] = ($entity->field_area->value == "homepage") ? $this->t("Homepage") : $this->t("All pages");
        $row['field_closed'] = ($entity->field_closed->value) ? $this->t("Yes") : $this->t("No");
        $row['field_date_on'] = $datetime_on;
        $row['field_date_off'] = $datetime_off;
        return $row + parent::buildRow($entity);
    }
}