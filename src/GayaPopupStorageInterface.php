<?php

namespace Drupal\gaya_popup;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\gaya_popup\Entity\GayaPopupInterface;

/**
 * Defines the storage handler class for Popup Messages Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Popup Messages Entity entities.
 *
 * @ingroup gaya_popup
 */
interface GayaPopupStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Popup Messages Entity revision IDs for a specific Popup Messages Entity.
   *
   * @param \Drupal\gaya_popup\Entity\GayaPopupInterface $entity
   *   The Popup Messages Entity entity.
   *
   * @return int[]
   *   Popup Messages Entity revision IDs (in ascending order).
   */
  public function revisionIds(GayaPopupInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Popup Messages Entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Popup Messages Entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\gaya_popup\Entity\GayaPopupInterface $entity
   *   The Popup Messages Entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(GayaPopupInterface $entity);

  /**
   * Unsets the language for all Popup Messages Entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
