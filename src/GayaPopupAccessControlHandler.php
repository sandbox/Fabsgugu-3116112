<?php

namespace Drupal\gaya_popup;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Popup Messages Entity entity.
 *
 * @see \Drupal\gaya_popup\Entity\GayaPopup.
 */
class GayaPopupAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\gaya_popup\Entity\GayaPopupInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished popup messages entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published popup messages entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit popup messages entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete popup messages entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add popup messages entity entities');
  }

}
