<?php

namespace Drupal\gaya_popup;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Popup Messages Entity entities.
 *
 * @ingroup gaya_popup
 */
class GayaPopupListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Popup Messages Entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\gaya_popup\Entity\GayaPopup */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gaya_popup_entity.edit_form',
      ['gaya_popup_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
