<?php

namespace Drupal\gaya_popup\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Popup Messages Entity entities.
 *
 * @ingroup gaya_popup
 */
class GayaPopupDeleteForm extends ContentEntityDeleteForm {


}
