<?php

namespace Drupal\gaya_popup\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Popup Messages Entity edit forms.
 *
 * @ingroup gaya_popup
 */
class GayaPopupForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\gaya_popup\Entity\GayaPopup */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $start_timestamp = 0;
    $end_timestamp = 9999999999;
    if($form_state->hasValue('field_date_on') && !empty($form_state->getValue('field_date_on')[0]['value'])){
      $start_timestamp = $form_state->getValue('field_date_on')[0]['value']->getTimestamp();
    }
    if($form_state->hasValue('field_date_off') && !empty($form_state->getValue('field_date_off')[0]['value'])){
      $end_timestamp = $form_state->getValue('field_date_off')[0]['value']->getTimestamp();
    }
    if($end_timestamp < $start_timestamp){
      $form_state->setErrorByName('field_date_off', $this->t('End date must be greater or equal to start date'));
    }

    if(isset($values['field_scheduled']) && $values['field_scheduled']['value'] == '1'){
      $field_scheduled_begin = $values['field_scheduled_begin'][0]['value'];
      $field_scheduled_end = $values['field_scheduled_end'][0]['value'];

      $regexp = "/^(([0-1][0-9]|2[0-3]):[0-5][0-9])$/"; // This will get from 00:00 to 23:59

      if(!preg_match($regexp,$field_scheduled_begin)){
        $form_state->setErrorByName('field_scheduled_begin', $this->t('Invalid time format HH:mm'));
      }
      if(!preg_match($regexp,$field_scheduled_end)){
        $form_state->setErrorByName('field_scheduled_end', $this->t('Invalid time format HH:mm'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(REQUEST_TIME);
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Popup Messages Entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Popup Messages Entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('gaya_popup.popup_entity_settings');
  }

}
