<?php

namespace Drupal\gaya_popup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class ContentEntityExampleSettingsForm.
 * @package Drupal\gaya_popup\Form
 * @ingroup gaya_popup_entity
 */
class PopupEntitySettingsForm extends FormBase {
    /**
     * Returns a unique string identifying the form.
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId() {
        return 'gaya_popup_entity_settings';
    }


    public function buildForm(array $form, FormStateInterface $form_state) {
        $resultsdb = \Drupal::service('database')
            ->select('gaya_popup_entity_field_data', 'gpefd')
            ->fields('gpefd',['id'])
            ->condition('langcode', 'fr')
            ->orderBy('gpefd.weight','ASC')
            ->execute();
        $results = $resultsdb->fetchCol();

        $user = User::load(\Drupal::currentUser()->id());
        if($user->hasPermission('edit popup message entity')){
            $form['link_addnew'] = [
                '#title' => $this->t('Add Popup Message Entity'),
                '#type' => 'link',
                '#url' => \Drupal\Core\Url::fromUserInput('/admin/structure/gaya_popup_entity/add'),
                '#attributes' => [
                    'class' => ['button button-action button--primary button--small'],
                    'data-drupal-link-system-path' => 'gaya_popup_entity/add'
                ],
                '#prefix' => "<ul class='action-links'><li>",
                '#suffix' => "</li></ul>",
            ];

            $form['description'] = [
                '#markup' => $this->t('<a href="@adminlink">Default alerts entities view</a>', array(
                    '@adminlink' => \Drupal::urlGenerator()
                        ->generateFromRoute('entity.gaya_popup_entity.collection'),
                )),
                '#prefix' => "<div>",
                '#suffix' => "</div>",
            ];

            $form['global_settings'] = [
                '#markup' => $this->t('<a href="@adminlink">Global settings for alerts</a>', array(
                    '@adminlink' => \Drupal::urlGenerator()
                        ->generateFromRoute('gaya_popup.popup_entity_settings'),
                )),
                '#prefix' => "<div>",
                '#suffix' => "</div>",
            ];
        }


        if(empty($results)){
            return $form;
        }
        $header['id'] = $this->t('#id');
        $header['title'] = $this->t('Title');
        $header['field_message'] = $this->t('Message');
        $header['status'] = $this->t('Status');
        $header['field_frequency'] = $this->t('Frequency');
        $header['field_area'] = $this->t('Area');
        $header['field_closed'] = $this->t('Hide if closed');
        $header['field_date_on'] = $this->t('Date de debut');
        $header['field_date_off'] = $this->t('Date de fin');
        $header['field_scheduled'] = $this->t('Scheduled');
        $header['field_scheduled_begin'] = $this->t('Heure de debut');
        $header['field_scheduled_end'] = $this->t('Heure de fin');
        $header['weight'] = $this->t('Poids');
        $header['operations'] = $this->t('Operations');

        $rows = [];
        foreach ($results as $popup_id) {
            $entity_storage = \Drupal::entityTypeManager()->getStorage('gaya_popup_entity');
            $entity = $entity_storage->load($popup_id);

            $datetime_on = "";
            if(!empty($entity->field_date_on->value)){
                $datetime_on = strtotime($entity->field_date_on->value);
                $datetime_on = \Drupal::service('date.formatter')->format($datetime_on, 'custom', 'Y-m-d');
            }
            $datetime_off = "";
            if(!empty($entity->field_date_off->value)){
                $datetime_off = strtotime($entity->field_date_off->value);
                $datetime_off = \Drupal::service('date.formatter')->format($datetime_off, 'custom', 'Y-m-d');
            }

            $rows[] = [
                "id" => $entity->id(),
                "title" => $entity->label(),
                "field_message" => $entity->field_message->value,
                "status" =>  $entity->status->value,
                "field_frequency" => ($entity->field_frequency->value == 'repeat') ? $this->t("Everytime") : $this->t("Once in session"),
                "field_area" => ($entity->field_area->value == "homepage") ? $this->t("Homepage") : $this->t("All pages"),
                "field_closed" => ($entity->field_closed->value) ? $this->t("Yes") : $this->t("No"),
                "field_date_on" => $datetime_on,
                "field_date_off" => $datetime_off,
                "field_scheduled" => ($entity->field_scheduled->value) ? $this->t("Yes") : $this->t("No"),
                "field_scheduled_begin" => $entity->field_scheduled_begin->value,
                "field_scheduled_end" => $entity->field_scheduled_end->value,
                "weight" => $entity->weight->value,
            ];
        }

        $form['popup_list'] = [
            '#type' => 'table',
            '#header' => $header,
            '#title' => t('Popup messages'),
            '#empty' => $this->t('There are no popup messages!'),
            '#tabledrag' => [
                [
                    'action' => 'order',
                    'relationship' => 'sibling',
                    'group' => 'table-sort-weight',
                ],
            ]
        ];

        for ($i = 0; $i < count($rows);$i++){
            $form['popup_list'][$i]['#attributes']['class'][] = 'draggable';
            $form['popup_list'][$i]['#weight'] = $i;

            $form['popup_list'][$i]['id'] = [
                '#type' => 'value',
                '#markup' => $rows[$i]['id'],
                '#default_value' => $rows[$i]['id'],
                '#value' => $rows[$i]['id'],
            ];
            $form['popup_list'][$i]['title'] = [
                '#markup' => $rows[$i]['title']
            ];
            $form['popup_list'][$i]['field_message'] = [
                '#markup' => $rows[$i]['field_message']
            ];
            $form['popup_list'][$i]['status'] = [
                '#markup' => ($rows[$i]['status']) ? $this->t("Yes") : $this->t("No")
            ];
            $form['popup_list'][$i]['field_frequency'] = [
                '#markup' => $rows[$i]['field_frequency']
            ];
            $form['popup_list'][$i]['field_area'] = [
                '#markup' => $rows[$i]['field_area']
            ];
            $form['popup_list'][$i]['field_closed'] = [
                '#markup' => $rows[$i]['field_closed']
            ];
            $form['popup_list'][$i]['field_date_on'] = [
                '#markup' => $rows[$i]['field_date_on']
            ];
            $form['popup_list'][$i]['field_date_off'] = [
                '#markup' => $rows[$i]['field_date_off']
            ];
            $form['popup_list'][$i]['field_scheduled'] = [
                '#markup' => $rows[$i]['field_scheduled']
            ];
            $form['popup_list'][$i]['field_scheduled_begin'] = [
                '#markup' => $rows[$i]['field_scheduled_begin']
            ];
            $form['popup_list'][$i]['field_scheduled_end'] = [
                '#markup' => $rows[$i]['field_scheduled_end']
            ];
            $form['popup_list'][$i]['weight'] = [
                '#type' => 'weight',
                '#title' => $this->t('Weight for @title', ['@title' => $rows[$i]['title']]),
                '#title_display' => 'invisible',
                '#default_value' => $i,
                '#attributes' => ['class' => ['table-sort-weight']],
            ];

            $id = $rows[$i]['id'];
            $markup = "<a href='/admin/structure/gaya_popup_entity/".$id."/edit?destination=/admin/content/gaya_popup_entity' hreflang='und'>".$this->t('Modifier')."</a>";
            if($rows[$i]['status'] == '1'){
                $markup .= " <a href='/gaya_popup/disable/".$id."'>".$this->t('Disable')."</a>";
            }
            if($rows[$i]['status'] == '0'){
                $markup .= " <a href='/gaya_popup/enable/".$id."'>".$this->t('Enable')."</a>";
            }

            $form['popup_list'][$i]['operations'] = [
                '#markup' => $markup
            ];
        }

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
        ];

        $formStorage = $form_state->getStorage();
        $form_state->setStorage($formStorage);

        return $form;
    }


    public function submitForm(array &$form, FormStateInterface $form_state) {
        $formStorage = $form_state->getValues();
        $datas = $formStorage['popup_list'];
        $entity_storage = \Drupal::entityTypeManager()->getStorage('gaya_popup_entity');

        $default_values = [];
        for ($ind = 0; $ind < count($datas); $ind++){
            $pitem = $form['popup_list'][$ind];
            if(!empty($pitem['id']['#value'])){
                $default_values[$pitem['id']['#value']] = $pitem['weight']['#value'];
            }
        }

        foreach ($datas as $data){
            $popupmessage = $entity_storage->load($data['id']);
            $popupmessage->set('weight', $data['weight']);
            $popupmessage->save();
        }

        drupal_set_message(t('The order of popup messages has been updated.'));
    }
}