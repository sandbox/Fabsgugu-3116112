<?php

namespace Drupal\gaya_popup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GayaPopupSettingsForm.
 *
 * @ingroup gaya_popup
 */
class GayaPopupSettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'gayapopup_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gaya_popup.settings'];
  }

  /**
   * Defines the settings form for Popup Messages Entity entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['gayapopup_settings']['#markup'] = 'Settings form for Popup Messages Entity entities. Manage field settings here.';
    $config = $this->config('gaya_popup.settings');
    $state = \Drupal::state();

    $form['popup_disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable all popups'),
      '#description' => $this->t('All Popup entities will be DISABLED when the checkbox is active.'),
      '#default_value' => $state->get('gaya_popup.settings.popup_disabled', 0),
    ];
    if (\Drupal::service('module_handler')->moduleExists('content_translation')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#open' => TRUE,
        '#tree' => TRUE,
      ];
      $form_state->set(['content_translation', 'key'], 'language');
      $form['language'] += content_translation_enable_widget('gaya_popup_entity', 'gaya_popup_entity', $form, $form_state);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $variables = [
      'popup_disabled' => $form_state->getValue('popup_disabled'),
    ];

    $config = $this->config('gaya_popup.settings');
    $state = \Drupal::state();
    foreach ($variables as $key => $value) {
      $config->set($key, $value);
      $state->set('gaya_popup.settings.' . $key, $value);
    }
    $config->save();
  }

}
