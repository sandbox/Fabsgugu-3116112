<?php

namespace Drupal\gaya_popup;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for gaya_popup_entity.
 */
class GayaPopupTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
