<?php

namespace Drupal\gaya_popup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GayaPopupBaseController extends ControllerBase {


  public function enableMessage($popupmessage) {
    if (!\Drupal::currentUser()->hasPermission('edit popup messages entity entities')) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    try {
      $popupmessage->set('status', '1');
      $popupmessage->save();
    } catch (\Exception $e) {
      \Drupal::logger('popup_entity')->error($e->getMessage());
    }

    $response = new RedirectResponse(Url::fromRoute('gaya_popup.popup_entity_settings')->toString());
    $response->send();
  }

  public function disableMessage($popupmessage) {
    if (!\Drupal::currentUser()->hasPermission('edit popup messages entity entities')) {
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    try {
      $popupmessage->set('status', '0');
      $popupmessage->save();
    } catch (\Exception $e) {
      \Drupal::logger('popup_entity')->error($e->getMessage());
    }

    $response = new RedirectResponse(Url::fromRoute('gaya_popup.popup_entity_settings')->toString());
    $response->send();
  }

  public function getPopupMessage() {
    $state = \Drupal::state();
    $popup_disabled = $state->get('gaya_popup.settings.popup_disabled', 0);

    if ($popup_disabled) {
      return new \Symfony\Component\HttpFoundation\JsonResponse("");
    }

    $homepage = \Drupal::request()->query->get('homepage', 'fr');

    $timezone = drupal_get_user_timezone();
    $date = new \DateTime('now', new \DateTimezone($timezone));
    $date->setTimezone(new \DateTimeZone(DATETIME_STORAGE_TIMEZONE));
    $date = DrupalDateTime::createFromDateTime($date);
    $date = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $results = \Drupal::entityQuery('gaya_popup_entity')
      ->sort('weight', 'ASC')
      ->condition('field_date_on', $date, '<=')
      ->condition('field_date_off', $date, '>=')
      ->condition('status', 1, '=')
      ->condition('langcode', \Drupal::languageManager()->getCurrentLanguage()->getId())
      ->execute();
    $result = $this->filterScheduled($results);
    if (empty($result)) {
      return [];
    }

    $popup_entity = \Drupal::entityTypeManager()->getStorage('gaya_popup_entity')->load($result);

    if($popup_entity->get('field_area')->value == 'homepage' && !$homepage){
      return [];
    }

    $entity = \Drupal::entityTypeManager()->getViewBuilder('gaya_popup_entity')->view($popup_entity, 'full');
    return new \Symfony\Component\HttpFoundation\JsonResponse(render($entity));
  }

  public function filterScheduled($results) {
    if (empty($results)) {
      return [];
    }
    $current_time = time();
    foreach ($results as $result) {
      $popup_entity = \Drupal::entityTypeManager()->getStorage('gaya_popup_entity')->load($result);
      if ($popup_entity->hasField('field_scheduled')) {
        if ($popup_entity->field_scheduled->value != '1') {
          return $result;
        }
        else {
          if (!$popup_entity->hasField('field_scheduled_begin') || !$popup_entity->hasField('field_scheduled_end')) {
            continue;
          }
          $begin = empty($popup_entity->field_scheduled_begin->value) ? 0 : $popup_entity->field_scheduled_begin->value;
          $end = empty($popup_entity->field_scheduled_end->value) ? 0 : $popup_entity->field_scheduled_end->value;
          if($begin < $end){
            if ((strtotime($begin) <= $current_time) && (strtotime($end) >= $current_time)) {
              return $result;
            }
          }
          else{
            if ((strtotime($begin) >= $current_time) || (strtotime($end) <= $current_time)) {
              return $result;
            }
          }

        }
      }
    }
  }
}
