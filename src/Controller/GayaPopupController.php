<?php

namespace Drupal\gaya_popup\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gaya_popup\Entity\GayaPopupInterface;

/**
 * Class GayaPopupController.
 *
 *  Returns responses for Popup Messages Entity routes.
 */
class GayaPopupController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Popup Messages Entity  revision.
   *
   * @param int $gaya_popup_entity_revision
   *   The Popup Messages Entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($gaya_popup_entity_revision) {
    $gaya_popup_entity = $this->entityManager()->getStorage('gaya_popup_entity')->loadRevision($gaya_popup_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('gaya_popup_entity');

    return $view_builder->view($gaya_popup_entity);
  }

  /**
   * Page title callback for a Popup Messages Entity  revision.
   *
   * @param int $gaya_popup_entity_revision
   *   The Popup Messages Entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($gaya_popup_entity_revision) {
    $gaya_popup_entity = $this->entityManager()->getStorage('gaya_popup_entity')->loadRevision($gaya_popup_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $gaya_popup_entity->label(), '%date' => format_date($gaya_popup_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Popup Messages Entity .
   *
   * @param \Drupal\gaya_popup\Entity\GayaPopupInterface $gaya_popup_entity
   *   A Popup Messages Entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(GayaPopupInterface $gaya_popup_entity) {
    $account = $this->currentUser();
    $langcode = $gaya_popup_entity->language()->getId();
    $langname = $gaya_popup_entity->language()->getName();
    $languages = $gaya_popup_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $gaya_popup_entity_storage = $this->entityManager()->getStorage('gaya_popup_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gaya_popup_entity->label()]) : $this->t('Revisions for %title', ['%title' => $gaya_popup_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all popup messages entity revisions") || $account->hasPermission('administer popup messages entity entities')));
    $delete_permission = (($account->hasPermission("delete all popup messages entity revisions") || $account->hasPermission('administer popup messages entity entities')));

    $rows = [];

    $vids = $gaya_popup_entity_storage->revisionIds($gaya_popup_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gaya_popup\GayaPopupInterface $revision */
      $revision = $gaya_popup_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gaya_popup_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.gaya_popup_entity.revision', ['gaya_popup_entity' => $gaya_popup_entity->id(), 'gaya_popup_entity_revision' => $vid]));
        }
        else {
          $link = $gaya_popup_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gaya_popup_entity.translation_revert', ['gaya_popup_entity' => $gaya_popup_entity->id(), 'gaya_popup_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gaya_popup_entity.revision_revert', ['gaya_popup_entity' => $gaya_popup_entity->id(), 'gaya_popup_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gaya_popup_entity.revision_delete', ['gaya_popup_entity' => $gaya_popup_entity->id(), 'gaya_popup_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gaya_popup_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
