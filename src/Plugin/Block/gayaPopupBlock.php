<?php

namespace Drupal\gaya_popup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\gaya_popup\Controller\GayaPopupBaseController;
/**
 * Provides a 'Gaya Popup' block.
 *
 * @Block(
 *   id = "block_gaya_popup",
 *   admin_label = @Translation("Gaya Popup block"),
 *   category = @Translation("Gaya Popup block")
 * )
 */
class gayaPopupBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function build() {
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $route = \Drupal::routeMatch()->getRouteObject();
        $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);
        if($is_admin) return [];

        $state = \Drupal::state();
        //$langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $popup_disabled = $state->get('gaya_popup.settings.popup_disabled', 0);

        if($popup_disabled) return [];

        $is_homepage = \Drupal::service('path.matcher')->isFrontPage();

        $date = date('Y-m-d', time());

        $langcode = empty($langcode) ? 'fr' : $langcode;
      $results = \Drupal::entityQuery('gaya_popup_entity')
          ->condition('field_date_on', $date, '<=')
          ->condition('field_date_off', $date, '>=')
          ->condition('status', '1', '=')
          ->sort('weight', 'ASC')
          ->execute();

        $popup_controller = new GayaPopupBaseController();
        $result = $popup_controller->filterScheduled($results);

        if(empty($result)) return [];

        $popup_entity = \Drupal::entityTypeManager()->getStorage('gaya_popup_entity')->load($result);

        $date_on = $popup_entity->field_date_on->value;
        $start_timestamp = strtotime($date_on);

        $date_off = $popup_entity->field_date_off->value;
        $end_timestamp = strtotime($date_off);

        $popup_page = $popup_entity->field_area->value;
        if(!$is_homepage && $popup_page == 'homepage') return [];

        $popup_frequency = $popup_entity->field_frequency->value;

        $body =  $popup_entity->field_message->value;
        \Drupal::service('page_cache_kill_switch')->trigger();
        return [
            '#theme' => 'block__gaya_popup',
            '#body' => $body,
            '#area' => $popup_page,
            '#frequency' => $popup_frequency,
            '#date_on' => $start_timestamp,
            '#date_off' => $end_timestamp,
            '#popupid' => $popup_entity->uuid->value,
            '#closed' => $popup_entity->field_closed->value,
            '#attached' => [
                'library' => [
                    'gaya_popup/gaya_popup.assets'
                ]
            ],
            '#cache' => [
                'max-age' => 0,
            ]
        ];
    }
}
