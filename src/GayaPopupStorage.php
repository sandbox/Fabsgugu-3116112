<?php

namespace Drupal\gaya_popup;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\gaya_popup\Entity\GayaPopupInterface;

/**
 * Defines the storage handler class for Popup Messages Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Popup Messages Entity entities.
 *
 * @ingroup gaya_popup
 */
class GayaPopupStorage extends SqlContentEntityStorage implements GayaPopupStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(GayaPopupInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {gaya_popup_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {gaya_popup_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(GayaPopupInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {gaya_popup_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('gaya_popup_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
