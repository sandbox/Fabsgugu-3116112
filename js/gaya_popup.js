/**
 * @file
 * Popup behaviors.
 */

(function (window, $) {
  'use strict';
  $(document).ready(function () {
    if ($('#povover').length === 0) {
      $.ajax({
        url: drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + 'gaya_popup/ajax/getmessage?homepage=' + drupalSettings.homepage,
        method: 'GET',
        dataType: 'json',
        async: true,
        success: function (result) {
          $('footer').after(result);
          var $element = $('#popover');
          var popupId = $element.data('popupid');

          // If already see dont display if option checked
          var popupStatus = sessionStorage.getItem('popupStatus' + popupId) ? sessionStorage.getItem('popupStatus' + popupId) : false;
          var frequency = $element.data('frequency');
          if (popupStatus && frequency === 'once') {
            return;
          }

          // If already closed dont display if option checked
          var popupClosed = sessionStorage.getItem('popupClosed' + popupId) ? sessionStorage.getItem('popupClosed' + popupId) : false;
          var ifClosed = $element.data('closed');
          if (ifClosed === 1 && popupClosed) {
            return;
          }

          // For update message in cached block
          $element.addClass('on').addClass('popover');
          sessionStorage.setItem('popupStatus' + popupId, true);
        }
      });
    }
  });

  $(document).on('click', '#popover .btn-close', function () {
    $('#popover').toggleClass('on');
    var $popupElement = $(this).parent();
    var popupId = $popupElement.data('popupid');
    sessionStorage.setItem('popupClosed' + popupId, true);
  });
})(window, jQuery);
