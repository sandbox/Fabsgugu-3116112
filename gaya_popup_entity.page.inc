<?php

/**
 * @file
 * Contains gaya_popup_entity.page.inc.
 *
 * Page callback for Popup Messages Entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Popup Messages Entity templates.
 *
 * Default template: gaya_popup_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_gaya_popup_entity(array &$variables) {
  // Fetch GayaPopup Entity Object.
  $gaya_popup_entity = $variables['elements']['#gaya_popup_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
